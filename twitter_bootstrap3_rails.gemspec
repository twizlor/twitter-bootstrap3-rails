$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "twitter_bootstrap3_rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "twitter_bootstrap3_rails"
  s.version     = TwitterBootstrap3Rails::VERSION
  s.authors     = ["Karl Grogan"]
  s.email       = ["krlgrgn@gmail.com"]
  s.homepage    = ""
  s.summary     = "Basic Rails Engine for Twitter Bootstrap 3"
  s.description = "Basic Rails Engine for Twitter Bootstrap 3"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.0"
end
